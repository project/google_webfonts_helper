CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

[Google Webfonts Helper](https://google-webfonts-helper.herokuapp.com/fonts) module - integration with the service of the same name, which allows you to download Google fonts for local use.

This module will allow you to add fonts from the Google Fonts service using the administrative interface, and the module will download files and generate the corresponding library. In this way, you will be able to deliver fonts from the site server or CDN, which will speed up the site.

REQUIREMENTS
------------

- symfony/finder


INSTALLATION
------------

 * Download it using composer `composer require drupal/google_webfonts_helper`.
 * Install the Google Webfonts Helper module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


USAGE
-------------

 - After you install module, go to module settings: `/admin/config/system/google-webfonts-helper`
 - Add new fonts as much as you want.
 - It will download the files and generate stylesheet. The stylesheet must be attached using library `google_webfonts_helper/[webfont_id]`. You can copy-past it from admin page.
 - After you attach the library, you your new font(s)!

MAINTAINERS
-----------

 * Nikita Malyshev (Niklan) - https://www.drupal.org/u/niklan

