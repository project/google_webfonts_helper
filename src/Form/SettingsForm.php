<?php

namespace Drupal\google_webfonts_helper\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides settings form for module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The Stream Wrapper Manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected StreamWrapperManagerInterface $streamWrapperManager
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_webfonts_helper_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['fonts_path'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Fonts path'),
      '#description' => new TranslatableMarkup('Path to store fonts. The path must be accessible from internet.'),
      '#required' => TRUE,
      '#default_value' => $this->config('google_webfonts_helper.settings')
        ->get('fonts_path'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $fonts_path = $form_state->getValue('fonts_path');
    $fonts_path_scheme = $this->streamWrapperManager->getScheme($fonts_path);
    if (!$this->streamWrapperManager->isValidScheme($fonts_path_scheme)) {
      $form_state->setErrorByName('fonts_path', $this->t('You have entered an invalid scheme.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('google_webfonts_helper.settings')
      ->set('fonts_path', $form_state->getValue('fonts_path'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_webfonts_helper.settings',
    ];
  }

}
