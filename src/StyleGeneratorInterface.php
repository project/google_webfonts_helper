<?php

namespace Drupal\google_webfonts_helper;

use Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface;

/**
 * Provides style generator.
 */
interface StyleGeneratorInterface {

  /**
   * Generates style file.
   *
   * @param \Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface $google_webfont
   *   The google webfont.
   * @param string $mode
   *   The style mode.
   *
   * @return bool
   *   TRUE if generated successfully, FALSE otherwise.
   */
  public function generate(GoogleWebfontInterface $google_webfont, string $mode = GoogleWebfontInterface::CSS_LEGACY);

}
