<?php

namespace Drupal\google_webfonts_helper\Handler\Form;

use Drupal;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface;
use Drupal\google_webfonts_helper\RestApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Google Webfont form.
 */
class GoogleWebfontForm extends EntityForm {

  /**
   * The fonts REST API.
   *
   * @var \Drupal\google_webfonts_helper\RestApiInterface
   */
  protected $restApi;

  /**
   * Constructs a new GoogleWebfontForm object.
   *
   * @param \Drupal\google_webfonts_helper\RestApiInterface $rest_api
   *   The fonts REST API.
   */
  public function __construct(RestApiInterface $rest_api) {
    $this->restApi = $rest_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('google_webfonts_helper.rest_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $wrapper_id = Html::getUniqueId($this->getFormId());
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => new TranslatableMarkup('Label for the webfont configuration.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\google_webfonts_helper\Entity\GoogleWebfont::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['font_id'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Font family'),
      '#required' => TRUE,
      '#options' => $this->getFontOptions($form_state),
      '#default_value' => $this->entity->get('font_id'),
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    if ($this->getFontId($form_state)) {
      $form = $this->buildVariantsForm($form, $form_state);
      $form = $this->buildSubsetsForm($form, $form_state);
    }

    $form['css_target'] = [
      '#type' => 'radios',
      '#title' => new TranslatableMarkup('CSS target'),
      '#options' => [
        GoogleWebfontInterface::CSS_LEGACY => new TranslatableMarkup('Best support'),
        GoogleWebfontInterface::CSS_MODERN => new TranslatableMarkup('Modern browsers'),
      ],
      '#default_value' => $this->entity->get('css_target'),
      '#required' => TRUE,
      '#description' => new TranslatableMarkup('Best support: eot, woff, woff2, ttf, svg. Modern browsers: woff, woff2.'),
    ];

    $form['css_weight'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Library CSS weight'),
      '#description' => new TranslatableMarkup('The weight in which styles will be added in library definition.'),
      '#options' => [
        'base' => 'base',
        'layout' => 'layout',
        'component' => 'component',
        'state' => 'state',
        'theme' => 'theme',
      ],
      '#default_value' => $this->entity->get('css_weight'),
      '#required' => TRUE,
    ];

    $form['font_display'] = [
      '#type' => 'select',
      '#title' => 'font-display',
      '#description' => new TranslatableMarkup('Which font-display technique use for those font loading.'),
      '#options' => [
        'auto' => 'auto',
        'block' => 'block',
        'swap' => 'swap',
        'fallback' => 'fallback',
        'optional' => 'optional',
      ],
      '#default_value' => $this->entity->get('font_display'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Gets font options.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The font options.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getFontOptions(FormStateInterface $form_state) {
    $font_options = $form_state->get('font_options');
    if (!$font_options) {
      $font_options = [];
      $fonts = $this->restApi->fetchFonts();

      if (!$fonts) {
        return $font_options;
      }

      foreach ($fonts as $font) {
        $font_options[$font['id']] = $font['family'];
      }
      ksort($font_options);
      $form_state->set('font_options', $font_options);
    }

    return $font_options;
  }

  /**
   * Gets font_id.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The font ID.
   */
  protected function getFontId(FormStateInterface $form_state) {
    return $this->entity->isNew() ? $form_state->getValue('font_id') : $this->entity->get('font_id');
  }

  /**
   * Builds variants selection.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   */
  public function buildVariantsForm(array $form, FormStateInterface $form_state) {
    $font_id = $this->getFontId($form_state);
    $font_info = $this->restApi->fetchFont($font_id);
    $variant_options = [];
    foreach ($font_info['variants'] as $variation) {
      $variation_label = $variation['fontWeight'] . ' ' . $variation['fontStyle'];
      $variant_options[$variation['id']] = $variation_label;
    }
    ksort($variant_options);
    $form['variants'] = [
      '#type' => 'checkboxes',
      '#title' => new TranslatableMarkup('Styles'),
      '#options' => $variant_options,
      '#required' => TRUE,
      '#default_value' => $this->entity->get('variants'),
    ];

    return $form;
  }

  /**
   * Builds subsets form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form.
   */
  public function buildSubsetsForm(array $form, FormStateInterface $form_state) {
    $font_id = $this->getFontId($form_state);
    $font_info = $this->restApi->fetchFont($font_id);
    $subset_options = [];
    foreach ($font_info['subsets'] as $subset) {
      $subset_options[$subset] = $subset;
    }
    ksort($subset_options);
    $form['subsets'] = [
      '#type' => 'checkboxes',
      '#title' => new TranslatableMarkup('Subsets'),
      '#options' => $subset_options,
      '#required' => TRUE,
      '#default_value' => $this->entity->get('subsets'),
    ];

    return $form;
  }

  public function actionsElement(array $form, FormStateInterface $form_state) {
    $element = parent::actionsElement($form, $form_state);
    return $element;
  }

  /**
   * Ajax callback for full form replace.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The AJAX response.
   */
  public function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fonts = $this->getFontOptions($form_state);
    $form_state->setValue('family', $fonts[$this->getFontId($form_state)]);

    $variants = array_filter($form_state->getValue('variants'), function ($item) {
      return is_string($item);
    });
    $form_state->setValue('variants', $variants);

    $subsets = array_filter($form_state->getValue('subsets'), function ($item) {
      return is_string($item);
    });
    $form_state->setValue('subsets', $subsets);

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? new TranslatableMarkup('Created new google webfont %label.', $message_args)
      : new TranslatableMarkup('Updated google webfont %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
