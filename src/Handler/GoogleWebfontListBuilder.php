<?php

namespace Drupal\google_webfonts_helper\Handler;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides Google Webfont entity list builder.
 */
class GoogleWebfontListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $row['label'] = new TranslatableMarkup('Label');
    $row['family'] = new TranslatableMarkup('Font family');
    $row['variants'] = new TranslatableMarkup('Variants');
    $row['subsets'] = new TranslatableMarkup('Subsets');
    $row['library'] = new TranslatableMarkup('Library');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  public function buildRow(EntityInterface $entity) {
    $row['label']['data'] = $entity->label() . ' (' . $entity->get('id') . ')';
    $row['family']['data'] = $entity->get('family') . ' (' . $entity->get('font_id') . ')';
    $row['variants']['data'] = implode(', ', $entity->get('variants'));
    $row['subsets']['data'] = implode(', ', $entity->get('subsets'));
    $row['library']['data'] = 'google_webfonts_helper/' . $entity->id();
    $row['operations']['data'] = $this->buildOperations($entity);

    return $row;
  }

}
