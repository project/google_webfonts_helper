<?php

namespace Drupal\google_webfonts_helper;

/**
 * Provides font downloader.
 */
interface FontDownloaderInterface {

  /**
   * Downloads the archive with fonts.
   *
   * @param string $font_id
   *   The font ID.
   * @param array $variants
   *   The font variants.
   * @param array $subsets
   *   The font subsets.
   *
   * @return string|null
   *   The URI to downloaded archive. NULL if problem happens.
   *
   * @throws \Exception
   */
  public function download(string $font_id, array $variants, array $subsets);

  /**
   * Extracts archive with fonts.
   *
   * @param string $archive_filepath
   *   The archive filepath.
   * @param string $destination
   *   The destination path.
   *
   * @return array
   *   The list of files extracted from archive.
   *
   * @throws \Exception
   */
  public function extract(string $archive_filepath, string $destination);

}
