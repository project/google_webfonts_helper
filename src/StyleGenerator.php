<?php

namespace Drupal\google_webfonts_helper;

use Drupal\Core\Render\RendererInterface;
use Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface;
use Twig\Environment;

/**
 * Provides default implementation for StyleGeneratorInterface.
 */
class StyleGenerator implements StyleGeneratorInterface {

  /**
   * The file system manager.
   *
   * @var \Drupal\google_webfonts_helper\FileSystemManagerInterface
   */
  protected $fileSystemManager;

  /**
   * The REST API.
   *
   * @var \Drupal\google_webfonts_helper\RestApiInterface
   */
  protected $restApi;

  /**
   * The TWIG environment.
   *
   * @var \Twig\Environment
   */
  protected $twigEnvironment;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new StyleGenerator object.
   *
   * @param \Drupal\google_webfonts_helper\FileSystemManagerInterface $file_system_manager
   *   The file system manager.
   * @param \Drupal\google_webfonts_helper\RestApiInterface $rest_api
   *   The REST API.
   * @param \Twig\Environment $twig_environment
   *   The Twig environment.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(FileSystemManagerInterface $file_system_manager, RestApiInterface $rest_api, Environment $twig_environment, RendererInterface $renderer) {
    $this->fileSystemManager = $file_system_manager;
    $this->restApi = $rest_api;
    $this->twigEnvironment = $twig_environment;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(GoogleWebfontInterface $google_webfont, string $mode = GoogleWebfontInterface::CSS_LEGACY) {
    $font_variants = $this->fileSystemManager->validateFonts($google_webfont->id());
    if (empty($font_variants)) {
      return FALSE;
    }

    $font_info = $this->restApi->fetchFont($google_webfont->get('font_id'));
    foreach ($font_variants as $variant => $variant_files) {
      $variant_info = array_filter($font_info['variants'], function ($item) use ($variant) {
        return $item['id'] == $variant;
      });
      $variant_info = reset($variant_info);
      if (!$variant_info) {
        continue;
      }

      $font_variants[$variant]['font_family'] = str_replace("'", '', $variant_info['fontFamily']);
      $font_variants[$variant]['font_style'] = $variant_info['fontStyle'];
      $font_variants[$variant]['font_weight'] = $variant_info['fontWeight'];
      // Local names can be missing for some fonts. E.g. 'Fira Code'.
      if (isset($variant_info['local'])) {
        $font_variants[$variant]['local'] = $variant_info['local'];
      }
    }

    $build = [
      '#theme' => 'google_webfonts_helper_style',
      '#mode' => $mode,
      '#font_variants' => $font_variants,
      '#font_display' => $google_webfont->get('font_display'),
    ];

    // Disable debug while rendering styles. This will remove HTML debug
    // comments from styles.
    if ($this->twigEnvironment->isDebug()) {
      $this->twigEnvironment->disableDebug();
      $style = $this->renderer->renderPlain($build);
      $this->twigEnvironment->enableDebug();
    }
    else {
      $style = $this->renderer->renderPlain($build);
    }

    file_put_contents($this->fileSystemManager->getFontStylePathname($google_webfont->id()), $style);

    return TRUE;
  }

}
