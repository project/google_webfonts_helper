<?php

namespace Drupal\google_webfonts_helper\Entity;

/**
 * Provides Google Webfont entity.
 */
interface GoogleWebfontInterface {

  /**
   * Indicates CSS with legacy support.
   *
   * @var string
   */
  const CSS_LEGACY = 'legacy';

  /**
   * Indicates CSS only for modern browsers.
   *
   * @var string
   */
  const CSS_MODERN = 'modern';

  /**
   * Validates files.
   *
   * If fonts is missing, it will be downloaded. If style file is missing, it
   * will be generated.
   *
   * @param bool $force
   *   TRUE to force download fonts and generate styles, even if they presented.
   */
  public function validateFiles(bool $force = FALSE);

}
