<?php

namespace Drupal\google_webfonts_helper\Entity;

use Drupal;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines Google Webfont entity type.
 *
 * @ConfigEntityType(
 *   id = "google_webfont",
 *   label = @Translation("Google Webfont"),
 *   handlers = {
 *     "list_builder" =
 *   "Drupal\google_webfonts_helper\Handler\GoogleWebfontListBuilder",
 *     "form" = {
 *       "add" =
 *   "Drupal\google_webfonts_helper\Handler\Form\GoogleWebfontForm",
 *       "edit" =
 *   "Drupal\google_webfonts_helper\Handler\Form\GoogleWebfontForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   admin_permission = "administer google_webfonts_helper",
 *   links = {
 *     "collection" = "/admin/config/system/google-webfonts-helper",
 *     "add-form" = "/admin/config/system/google-webfonts-helper/add",
 *     "edit-form" =
 *   "/admin/config/system/google-webfonts-helper/{google_webfont}",
 *     "delete-form" =
 *   "/admin/config/system/google-webfonts-helper/{google_webfont}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "font_id",
 *     "family",
 *     "variants",
 *     "subsets",
 *     "css_target",
 *     "css_weight",
 *   }
 * )
 */
class GoogleWebfont extends ConfigEntityBase implements GoogleWebfontInterface {

  /**
   * The config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Google Webfont ID.
   *
   * @var string
   */
  protected $font_id;

  /**
   * The Google Webfont family.
   *
   * @var string
   */
  protected $family;

  /**
   * The list of enabled variant ID's.
   *
   * @var array
   */
  protected $variants = [];

  /**
   * The list of enabled subsets.
   *
   * @var array
   */
  protected $subsets = [];

  /**
   * The target browsers for CSS.
   *
   * @var string
   */
  protected $css_target = self::CSS_LEGACY;

  /**
   * The library CSS file rule weight weight.
   *
   * @var string
   */
  protected $css_weight = 'base';

  /**
   * The 'font-display' for font definition.
   *
   * @var string
   */
  protected $font_display = 'swap';

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $force = FALSE;
    // If config changed, force update everything.
    if (isset($this->original) && $this !== $this->original) {
      $force = TRUE;
    }
    $this->validateFiles($force);
  }

  /**
   * {@inheritdoc}
   */
  public function validateFiles(bool $force = FALSE) {
    /** @var \Drupal\google_webfonts_helper\FontManagerInterface $font_manager */
    $font_manager = Drupal::service('google_webfonts_helper.font_manager');
    $font_manager->prepare($this->id(), $force);
  }

}
