<?php

namespace Drupal\google_webfonts_helper;

/**
 * Provides wrapper for Googl Webfonts Helper website REST API.
 *
 * @see https://github.com/majodev/google-webfonts-helper#rest-api
 */
interface RestApiInterface {

  /**
   * Get all available fonts.
   *
   * @return array
   *   The list of available fonts.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function fetchFonts();

  /**
   * Fetches extended form information.
   *
   * @param string $font_id
   *   The font id.
   * @param array $query
   *   The query params.
   *
   * @return array
   *   The font info.
   */
  public function fetchFont(string $font_id, array $query = []);

  /**
   * Downloads font archive.
   *
   * @param string $font_id
   *   The font ID.
   * @param array $query
   *   The query params.
   * @param string $download_path
   *   The download path.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function downloadFont(string $font_id, array $query, string $download_path);

}
