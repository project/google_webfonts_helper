<?php

namespace Drupal\google_webfonts_helper;

use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\File\FileSystemInterface;

/**
 * Default implementation for FontDowloadInterface.
 */
class FontDownloader implements FontDownloaderInterface {

  /**
   * The REST API.
   *
   * @var \Drupal\google_webfonts_helper\RestApiInterface
   */
  protected $restApi;

  /**
   * The file system manager.
   *
   * @var \Drupal\google_webfonts_helper\FileSystemManagerInterface
   */
  protected $fileSystemManager;

  /**
   * The archiver manager.
   *
   * @var \Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiverManager;

  /**
   * Constructs a new FontDownloader object.
   *
   * @param \Drupal\google_webfonts_helper\RestApiInterface $rest_api
   *   The REST API.
   * @param \Drupal\google_webfonts_helper\FileSystemManagerInterface $file_system_manager
   *   The file system manager.
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver_manager
   *   The archiver manager.
   */
  public function __construct(RestApiInterface $rest_api, FileSystemManagerInterface $file_system_manager, ArchiverManager $archiver_manager) {
    $this->restApi = $rest_api;
    $this->fileSystemManager = $file_system_manager;
    $this->archiverManager = $archiver_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function download(string $font_id, array $variants, array $subsets) {
    $directory = $this->fileSystemManager->getDownloadDirectory();
    $this->fileSystemManager->getFileSystem()->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $download_path = "{$directory}/{$font_id}.zip";
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $this->restApi->downloadFont($font_id, [
      'subsets' => implode(',', $subsets),
      'variants' => implode(',', $variants),
    ], $download_path);

    return $response->getStatusCode() == 200 ? $download_path : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function extract(string $archive_filepath, string $destination) {
    /** @var \Drupal\Core\Archiver\ArchiverInterface $archiver */
    $files = []; 
    $archiver = $this->archiverManager->getInstance(['filepath' => $archive_filepath]);
    // Extract files only if archiver instance is properly created.
    if ($archiver){
      $archiver->extract($destination);
      $files = $archiver->listContents();
    }
    $this->fileSystemManager->getFileSystem()->unlink($archive_filepath);
    return $files;
  }

}
