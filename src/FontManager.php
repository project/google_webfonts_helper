<?php

namespace Drupal\google_webfonts_helper;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface;

/**
 * Provides default implementation for FontManagerInterface.
 */
class FontManager implements FontManagerInterface {

  /**
   * The font storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fontStorage;

  /**
   * The font downloader.
   *
   * @var \Drupal\google_webfonts_helper\FontDownloaderInterface
   */
  protected $fontDownloader;

  /**
   * The style generator.
   *
   * @var \Drupal\google_webfonts_helper\StyleGeneratorInterface
   */
  protected $styleGenerator;

  /**
   * The file system manager.
   *
   * @var \Drupal\google_webfonts_helper\FileSystemManagerInterface
   */
  protected $fileSystemManager;

  /**
   * Constructs a new FontManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\google_webfonts_helper\FontDownloaderInterface $font_downloader
   *   The font downloader.
   * @param \Drupal\google_webfonts_helper\StyleGeneratorInterface $style_generator
   *   The style generator.
   * @param \Drupal\google_webfonts_helper\FileSystemManagerInterface $file_system_manager
   *   The file system manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FontDownloaderInterface $font_downloader, StyleGeneratorInterface $style_generator, FileSystemManagerInterface $file_system_manager) {
    $this->fontStorage = $entity_type_manager->getStorage('google_webfont');
    $this->fontDownloader = $font_downloader;
    $this->styleGenerator = $style_generator;
    $this->fileSystemManager = $file_system_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(string $google_webfont_id, bool $force = FALSE) {
    $google_webfont = $this->fontStorage->load($google_webfont_id);
    if (!$google_webfont instanceof GoogleWebfontInterface) {
      return FALSE;
    }
    if (!$this->fileSystemManager->prepareDirectory($google_webfont_id)) {
      return FALSE;
    }

    // Clear previously downloaded fonts and generated style.
    if ($force) {
      $font_directory = $this->fileSystemManager->getFontDirectory($google_webfont->id());
      $this->fileSystemManager->getFileSystem()->deleteRecursive($font_directory);
    }

    $download_status = TRUE;
    if (!count($this->fileSystemManager->validateFonts($google_webfont_id)) || $force) {
      $download_status = $this->download($google_webfont);
    }

    $style_status = TRUE;
    if (!$this->fileSystemManager->validateFontStyle($google_webfont_id) || $force) {
      $style_status = $this->generateStyle($google_webfont);
    }

    return $download_status && $style_status;
  }

  /**
   * {@inheritdoc}
   */
  public function download(GoogleWebfontInterface $google_webfont) {
    $font_id = $google_webfont->get('font_id');
    $variants = $google_webfont->get('variants');
    $subsets = $google_webfont->get('subsets');
    $archive = $this->fontDownloader->download($font_id, $variants, $subsets);
    if (!$archive) {
      return FALSE;
    }
    $font_directory = $this->fileSystemManager->getFontDirectory($google_webfont->id());
    $this->fontDownloader->extract($archive, $font_directory);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function generateStyle(GoogleWebfontInterface $google_webfont) {
    return $this->styleGenerator->generate($google_webfont, $google_webfont->get('css_target'));
  }

}
