<?php

namespace Drupal\google_webfonts_helper;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use GuzzleHttp\ClientInterface;

/**
 * Provides default implementation for RestApiInterface.
 */
class RestApi implements RestApiInterface {

  /**
   * The base URL to API.
   */
  const BASE_URL = 'https://gwfh.mranftl.com';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a new RestApi object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $http_client, CacheBackendInterface $cache_backend) {
    $this->httpClient = $http_client;
    $this->cache = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchFonts() {
    $fonts_cache = $this->cache->get('fonts');
    if (!$fonts_cache) {
      $response = $this->doRequest('/api/fonts');
      $fonts = Json::decode($response->getBody()->getContents());
      $this->cache->set('fonts', $fonts);
    }
    else {
      $fonts = $fonts_cache->data;
    }
    return $fonts;
  }

  /**
   * Dos requests to REST endpoint.
   *
   * @param string $endpoint
   *   The REST endpoint.
   * @param array $options
   *   The request options.
   *
   * @return \Psr\Http\Message\ResponseInterface
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function doRequest(string $endpoint, array $options = []) {
    return $this->httpClient->request('GET', self::BASE_URL . $endpoint, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchFont(string $font_id, array $query = []) {
    $font_cache = $this->cache->get("font.{$font_id}");
    if (!$font_cache) {
      $response = $this->doRequest("/api/fonts/{$font_id}", ['query' => $query]);
      $font = Json::decode($response->getBody()->getContents());
      $this->cache->set("font.{$font_id}", $font, 60 * 60);
    }
    else {
      $font = $font_cache->data;
    }
    return $font;
  }

  /**
   * {@inheritdoc}
   */
  public function downloadFont(string $font_id, array $query, string $download_path) {
    $query += ['download' => 'zip'];
    $resource = fopen($download_path, 'w+');

    return $this->doRequest("/api/fonts/{$font_id}", [
      'query' => $query,
      'sink' => $resource,
    ]);
  }

}
