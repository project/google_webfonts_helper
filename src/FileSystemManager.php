<?php

namespace Drupal\google_webfonts_helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Exception;
use Symfony\Component\Finder\Finder;
use Drupal\Core\File\FileUrlGeneratorInterface;

/**
 * Provides default implementation for FileSystemManagerInterface.
 */
class FileSystemManager implements FileSystemManagerInterface {

  /**
   * The module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;


  /**
   * Constructs a new FileSystemManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, ?FileUrlGeneratorInterface $file_url_generator = NULL) {
    $this->settings = $config_factory->get('google_webfonts_helper.settings');
    $this->fileSystem = $file_system;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareDirectory(string $google_webfont_id) {
    $directory = $this->getFontDirectory($google_webfont_id);
    return $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
  }

  /**
   * {@inheritdoc}
   */
  public function getFontDirectory(string $google_webfont_id) {
    return $this->settings->get('fonts_path') . '/' . $google_webfont_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadDirectory() {
    return 'temporary://google-webfonts-helper';
  }

  /**
   * {@inheritdoc}
   */
  public function validateFonts(string $google_webfont_id) {
    try {
      $finder = new Finder();
      $finder->in($this->getFontDirectory($google_webfont_id));
      $finder->name('*.ttf');
      $finder->name('*.woff');
      $finder->name('*.woff2');
      $finder->name('*.svg');
      $finder->name('*.eot');
    } catch (Exception $exception) {
      return [];
    }

    $font_variants = [];
    foreach ($finder as $item) {
      $file_info = $this->parseFontFilename($item->getFilename());
      $font_variants[$file_info['variant']][$file_info['extension']] = $this->fileUrlGenerator->generateString($item->getPathname());
    }
    return $font_variants;
  }

  /**
   * Parses font filename.
   *
   * @param string $filename
   *   The font filename.
   *
   * @return array
   *   An array with font name info parsed.
   */
  protected function parseFontFilename(string $filename) {
    preg_match('/^(.*)-(.*)-(.*)-(.*)\.(eot|(woff|woff2)|svg|ttf)$/m', $filename, $matches);
    return [
      'filename' => $matches[0],
      'font_id' => $matches[1],
      'version' => $matches[2],
      'subset' => $matches[3],
      'variant' => $matches[4],
      'extension' => $matches[5],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateFontStyle(string $google_webfont_id) {
    return file_exists($this->getFontStylePathname($google_webfont_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getFontStylePathname(string $google_webfont_id) {
    return $this->getFontDirectory($google_webfont_id) . "/{$google_webfont_id}.css";
  }

  /**
   * {@inheritdoc}
   */
  public function getFileSystem() {
    return $this->fileSystem;
  }

}
