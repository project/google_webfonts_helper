<?php

namespace Drupal\google_webfonts_helper;

use Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface;

/**
 * Provides font manager to combine other services in one.
 */
interface FontManagerInterface {

  /**
   * Prepare all necessary files for font.
   *
   * @param string $google_webfont_id
   *   The google webfont id.
   *
   * @param bool $force
   *   If set to TRUE fonts and style will be generated even if them exists.
   *
   * @return bool
   *   TRUE if both, download and style process complete. FALSE if one of them
   *   is failed.
   */
  public function prepare(string $google_webfont_id, bool $force = FALSE);

  /**
   * Downloads fonts.
   *
   * @param \Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface $google_webfont
   *   The google webfont.
   *
   * @return bool
   *   TRUE if font was downloaded and style generated, FALSE if some problem
   *   happens.
   *
   * @throws \Exception
   */
  public function download(GoogleWebfontInterface $google_webfont);

  /**
   * Generates font styles.
   *
   * @param \Drupal\google_webfonts_helper\Entity\GoogleWebfontInterface $google_webfont
   *   The foofle webfont.
   *
   * @return bool
   *   TRUE if generates successfully, FALSE otherwise.
   */
  public function generateStyle(GoogleWebfontInterface $google_webfont);

}
