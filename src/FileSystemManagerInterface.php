<?php

namespace Drupal\google_webfonts_helper;

/**
 * Provides service for working with file system.
 */
interface FileSystemManagerInterface {

  /**
   * Prepares directory for font files.
   *
   * @param string $google_webfont_id
   *   The font ID.
   *
   * @return bool
   *   TRUE if the directory exists (or was created) and is writable. FALSE
   *   otherwise.
   */
  public function prepareDirectory(string $google_webfont_id);

  /**
   * Gets path for specific font.
   *
   * @param string $google_webfont_id
   *   The font ID.
   *
   * @return string
   *   The path to fonts folder.
   */
  public function getFontDirectory(string $google_webfont_id);

  /**
   * Gets temporary directory for downloads.
   *
   * @return string
   *   Temporary directory URI.
   */
  public function getDownloadDirectory();

  /**
   * Validates that fonts are downloaded and presented.
   *
   * @param string $google_webfont_id
   *   The font ID to check.
   *
   * @return array
   *   Empty array if fonts not found or array with file info grouped by
   *   font varaints and extensions.
   */
  public function validateFonts(string $google_webfont_id);

  /**
   * Validates that font style file is exists.
   *
   * @param string $google_webfont_id
   *   The google webfont ID.
   *
   * @return bool
   *   TRUE if file exists, FALSE otherwise.
   */
  public function validateFontStyle(string $google_webfont_id);

  /**
   * Gets font style pathname.
   *
   * @param string $google_webfont_id
   *   The google webfont ID.
   *
   * @return string
   *   The path to style file.
   */
  public function getFontStylePathname(string $google_webfont_id);

  /**
   * Gets file system.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   The file system.
   */
  public function getFileSystem();

}
